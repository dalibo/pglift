# SPDX-FileCopyrightText: 2021 Dalibo
#
# SPDX-License-Identifier: GPL-3.0-or-later

from __future__ import annotations

from .main import cli

if __name__ == "__main__":
    cli(prog_name="pglift")
