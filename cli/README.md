<!--
SPDX-FileCopyrightText: 2024 Dalibo

SPDX-License-Identifier: GPL-3.0-or-later
-->

This package provides the command-line interface for [pglift][]. It is a
dependency package and should not be installed directly but rather through the
``cli`` *optional dependency* (aka "extra") of the pglift package, e.g.:

    $ pip install "pglift[cli]"

[pglift]: https://pglift.readthedocs.io/
