.. SPDX-FileCopyrightText: 2021 Dalibo
..
.. SPDX-License-Identifier: GPL-3.0-or-later

Instances
=========

.. currentmodule:: pglift.instances

Module :mod:`pglift.instances` exposes the following API to manipulate
instances:

.. autofunction:: apply
.. autofunction:: get
.. autofunction:: exists
.. autofunction:: ls
.. autofunction:: drop
.. autofunction:: init
.. autofunction:: configure
.. autofunction:: start
.. autofunction:: stop
.. autofunction:: restart
.. autofunction:: reload
.. autofunction:: pending_restart
.. autofunction:: stopped
.. autofunction:: promote
.. autofunction:: upgrade
.. autofunction:: settings
