.. SPDX-FileCopyrightText: 2024 Dalibo
..
.. SPDX-License-Identifier: GPL-3.0-or-later

.. _cli_locking:

Concurrent operations in the command-line interface
===================================================

pglift's CLI uses an internal lock mechanism to prevent some commands to run
concurrently. It's mostly for operations that alter the system (instance,
database or role creation, update or removal).

Thus, time-consuming operations (for example creating a database using
cloning) will possibly prevent users from doing other simple operations.

Currently this mechanism is very generic and users might be surprised to see a
"another operation is in progress" error message for operations that they
don't expect to conflict.

For the *locking* mechanism to work, pglift relies on a file. Even though not
recommended, the path to this file may be changed via site settings:


.. code-block:: yaml
   :caption: settings.yaml

    cli:
      lock_file: '/tmp/.pglift.lock'
