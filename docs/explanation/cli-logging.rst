.. SPDX-FileCopyrightText: 2024 Dalibo
..
.. SPDX-License-Identifier: GPL-3.0-or-later

.. _cli_logging:

Logging in the command-line interface
=====================================

When invoked without any specific option, any ``pglift`` command will log
messages to the standard error in form of ``<LEVEL> <message>`` lines. The
default threshold for displayed messages is ``INFO`` so that messages with
this severity or higher are displayed but those with a lower severity (like
``DEBUG`` messages) are not. To control this threshold, the ``-L,
--log-level`` should be used.

To log messages to a file, the ``-l, --log-file LOGFILE`` should be used.

When an unexpected error occurs during the invocation of a pglift command
(i.e. something that is not a user error but may be a bug), pglift will write
all messages logged during this invocation to a temporary debug file and
finally display its path in standard error:

.. code-block:: console

    $ pglift ...
    ERROR    an unexpected error occurred
    Error: an unexpected error occurred, this is probably a bug;
        details can be found at /tmp/pglift/1714116611.5114818.log

This debug file is meant for troubleshooting and helping developers to fix
bugs.  It will not persist if the command exits in an expected way (which may
be a success and a known/user error).

.. _cli_auditing:

Auditing change operations through the command-line interface
=============================================================

The command-line interface may be configured to record log messages from
change operations, i.e. all events occurring when changing (creating /
updating / deleting) a resource managed by pglift (i.e. an instance, a
database object, etc.). Read operations (such as listing objects, getting
object details or exporting data) are not recorded in a so-called *audit
log*.

This needs to be enabled through site settings by defining an ``audit`` key in
the ``cli`` section, e.g.:

.. code-block:: yaml
   :name: cli-audit-settings
   :caption: settings.yaml

    cli:
      audit:
        path: '/var/log/pglift/audit.log'

.. warning::
   This feature will also log command-line input, not just their output. This
   means, in particular, that any sentitive data (e.g. passwords) given to the
   command line will appear in the audit log file.

.. note::
   This feature is provided on a provisional basis waiting for further users
   feedback before eventually getting stabilized.
