# SPDX-FileCopyrightText: 2021 Dalibo
#
# SPDX-License-Identifier: GPL-3.0-or-later

FROM rockylinux:8
RUN dnf install -y https://download.postgresql.org/pub/repos/yum/reporpms/EL-8-x86_64/pgdg-redhat-repo-latest.noarch.rpm
RUN dnf -qy module disable postgresql
RUN dnf install -y epel-release
ARG PG_VERSION=13
RUN dnf install -y postgresql${PG_VERSION}-server \
	postgresql${PG_VERSION}-contrib \
	pg_stat_kcache${PG_VERSION} \
	pg_qualstats${PG_VERSION} \
	powa_${PG_VERSION} \
	pgbackrest
WORKDIR /tmp
ARG ETCD_VER=v3.5.4
ARG ETCD_DOWNLOAD_URL=https://storage.googleapis.com/etcd
RUN curl -O -L \
	${ETCD_DOWNLOAD_URL}/${ETCD_VER}/etcd-${ETCD_VER}-linux-amd64.tar.gz
RUN tar --no-same-owner --strip-components=1 -x -f etcd-${ETCD_VER}-linux-amd64.tar.gz
RUN mv etcd /usr/bin/
RUN mv etcdctl /usr/bin/
ARG PGE_VERSION=0.10.1
ARG PGE_FNAME=postgres_exporter-${PGE_VERSION}.linux-amd64
ARG PGE_TGZ=${PGE_FNAME}.tar.gz
RUN curl -O -L \
	https://github.com/prometheus-community/postgres_exporter/releases/download/v${PGE_VERSION}/${PGE_TGZ}
RUN tar --no-same-owner -x -f ${PGE_TGZ}
RUN mv ${PGE_FNAME}/postgres_exporter /usr/bin/
RUN dnf install -y gcc git jq openssl procps-ng python3.11 python3.11-pip \
	&& dnf clean all \
	&& rm -rf /var/cache/yum

RUN dnf install -y https://yum.dalibo.org/labs/dalibo-labs-4-1.noarch.rpm
RUN dnf install -y --repo=appstream python3-psycopg2
RUN dnf install -y temboard-agent
RUN dnf install -y 'dnf-command(versionlock)'
RUN dnf versionlock add python3-psycopg2

ARG PG_BACK_VERSION=2.3.0
RUN dnf install -y \
	https://github.com/orgrim/pg_back/releases/download/v${PG_BACK_VERSION}/pg-back-${PG_BACK_VERSION}-x86_64.rpm

RUN useradd --uid 5432 --create-home runner

RUN touch /etc/port-for.conf
RUN chown runner: /etc/port-for.conf

USER runner
WORKDIR /home/runner
RUN pip3.11 install --user uv
ENV PATH=/home/runner/.local/bin:$PATH
RUN uv tool install \
	--python /usr/bin/python3.11 --python-preference=system --no-python-downloads \
	tox --with tox-uv
ENV PATH=/usr/pgsql-${PG_VERSION}/bin:$PATH
