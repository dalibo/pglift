# SPDX-FileCopyrightText: 2021 Dalibo
#
# SPDX-License-Identifier: GPL-3.0-or-later

from __future__ import annotations

import os
import socket
import subprocess
from collections.abc import Callable, Iterator
from pathlib import Path

import psycopg
import pytest
import yaml
from psycopg.rows import dict_row

from ..etcd import Etcd
from . import convert_for_pydantic_envvar


@pytest.fixture(scope="package")
def patroni_execpaths(
    patroni_execpaths: tuple[Path, Path] | None,
) -> tuple[Path, Path]:
    if patroni_execpaths:
        return patroni_execpaths
    pytest.skip("Patroni is not available")


@pytest.fixture(scope="module")
def etcd_host(etcd_host: Etcd | None) -> Etcd:
    if etcd_host is None:
        pytest.skip("etcd executable not found")
    return etcd_host


@pytest.fixture(scope="module", autouse=True)
def _etcd_running(etcd_host: Etcd) -> Iterator[None]:
    with etcd_host.running():
        yield None


@pytest.fixture
def site_settings_env(
    tmp_path: Path,
    patroni_execpaths: tuple[Path, Path],
    etcd_host: Etcd,
    ca_cert: Path,
) -> Iterator[dict[str, str]]:
    patroni, patronictl = patroni_execpaths
    settings = {
        "prefix": str(tmp_path),
        "run_prefix": str(tmp_path / "run"),
        "patroni": {
            "execpath": str(patroni),
            "ctlpath": str(patronictl),
            "etcd": {
                "hosts": [etcd_host.endpoint],
                "protocol": "https",
                "cacert": str(ca_cert),
            },
        },
        "postgresql": {"replrole": "replication"},
    }
    env = convert_for_pydantic_envvar(settings)
    subprocess.run(
        ["pglift", "--debug", "site-configure", "install"],
        capture_output=True,
        check=True,
        env=os.environ | env,
    )
    yield env
    subprocess.run(
        ["pglift", "--non-interactive", "--debug", "site-configure", "uninstall"],
        capture_output=True,
        check=True,
        env=os.environ | env,
    )


@pytest.fixture
def primary_pgport(tmp_port_factory: Iterator[int]) -> int:
    return next(tmp_port_factory)


@pytest.fixture
def replica_pgport(tmp_port_factory: Iterator[int]) -> int:
    return next(tmp_port_factory)


@pytest.fixture
def call_playbook(
    tmp_path: Path,
    ansible_env: dict[str, str],
    playdir: Path,
    site_settings_env: dict[str, str],
    tmp_port_factory: Iterator[int],
    primary_pgport: int,
    replica_pgport: int,
) -> Iterator[Callable[[str], None]]:
    env = (
        os.environ
        | {"PGLIFT_CONFIG_DIR": str(Path(__file__).parent / "data")}
        | site_settings_env
    )
    vars = tmp_path / "vars"
    host = socket.gethostbyname(socket.gethostname())
    with vars.open("w") as f:
        yaml.safe_dump(
            {
                "primary": {
                    "postgresql_port": primary_pgport,
                    "rest_api": f"{host}:{next(tmp_port_factory)}",
                },
                "replica": {
                    "postgresql_port": replica_pgport,
                    "rest_api": f"{host}:{next(tmp_port_factory)}",
                },
            },
            f,
        )

    def call(playname: str) -> None:
        subprocess.check_call(
            [
                "ansible-playbook",
                "--extra-vars",
                f"@{vars}",
                playdir / playname,
            ],
            env=env,
        )

    yield call

    call("ha-delete.yml")


def test(
    call_playbook: Callable[[str], None], primary_pgport: int, replica_pgport: int
) -> None:
    call_playbook("ha-setup.yml")

    # test connection to the primary instance
    primary_dsn = f"host=/tmp user=postgres dbname=postgres port={primary_pgport}"
    with psycopg.connect(primary_dsn, row_factory=dict_row) as cnx:
        row = cnx.execute("SHOW work_mem").fetchone()
    assert row is not None

    # test connection to the replica instance
    replica_dsn = f"host=/tmp user=postgres dbname=postgres port={replica_pgport}"
    with psycopg.connect(replica_dsn, row_factory=dict_row) as cnx:
        row = cnx.execute("SHOW work_mem").fetchone()
    assert row is not None
