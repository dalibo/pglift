Fix backup of Patroni configuration file when dropping the last node of a
cluster when running with systemd (was broken since pglift 1.8.0).
