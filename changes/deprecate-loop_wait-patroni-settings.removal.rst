Deprecate the ``loop_wait`` settings for Patroni.

``bootstrap.dcs.loop_wait`` configuration should be done by
overriding the Patroni template.
