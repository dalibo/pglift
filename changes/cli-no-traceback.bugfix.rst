No longer output a traceback in case of unexpected error in command-line
operations (debug information will still be available in the dedicated log
file).
