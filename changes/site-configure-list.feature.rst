Add ``list`` action to ``site-configure`` command to output the list of files
managed during site configuration.
