Make the ``{version}`` variable in PostgreSQL ``datadir``, ``waldir`` and
``dumps_directory`` settings optional.
