Add a ``--dry-run`` option to ``pgconf {set,remove}`` commands.
