Query Patroni REST API over HTTPS when ``patroni.restapi.certfile`` and
``patroni.restapi.keyfile`` setting are set, as already done if
``patroni.restapi.cafile`` is set.
