Avoid error when failing to list database dumps if there is a file that doesn't
follow the expected format.
