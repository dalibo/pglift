===========================
dalibo.pglift Release Notes
===========================

.. contents:: Topics

v1.3.0
======

Minor Changes
-------------

- role - The ``validity`` field is deprecated in favour of ``valid_until``.

v1.2.0
======

Minor Changes
-------------

- Fill the stderr key of module result with log messages of the underlying pglift operation.
- Insert validation errors details in an "errors" key in the JSON output.
- role - A ``hba_records`` field has been added to manipulate entries in the ``pg_hba.conf`` file for the given role.

v1.1.0
======

Minor Changes
-------------

- instance - The ``port`` field no longer has an explicit default value in order to account for respective setting possibly defined in "postgresql.conf" site template.
- role - A ``memberships`` field has been added to declare role memberships with ``state`` field controlling ``GRANT``/``REVOKE`` effects.

Deprecated Features
-------------------

- role - The ``in_roles`` field is deprecated in favour of ``memberships``.

v1.0.6
======

v1.0.5
======

v1.0.4
======

Minor Changes
-------------

- Merge code repository into main pglift's repository, now at <https://gitlab.com/dalibo/pglift>.

v1.0.3
======

Minor Changes
-------------

- Properly declare license and copyright information in sources.

v1.0.2
======

Minor Changes
-------------

- Remove ``gss`` from local authentication methods, as it's only available for TCP/IP connections.

v1.0.1
======

Minor Changes
-------------

- Remove the external requirement on the `packaging` Python package.

v1.0.0
======

Major Changes
-------------

- Require pglift version 1.0.0a1 or higher.

Minor Changes
-------------

- Add ``EXAMPLES`` sections to modules documentation.
- Add a `CONTRIBUTING` document.
- Add a ``owner`` field to database's schema.
- Add a change log.
- Check that the version `pglift` available on target host is SemVer-compatible with declared requirements.
- Document external requirements in the README.

Removed Features (previously deprecated)
----------------------------------------

- Deprecated ``clone_from`` field of ``database`` module and ``databases.*.clone_from`` field of ``instance`` module have been removed.
- Deprecated ``patroni.postgresql_connect_host`` field of ``instance`` module has been removed.
